package exservlet.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.Valid;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Order {
    @Id
    @GeneratedValue
    private Long id;
    @Size(min = 2)
    private String orderNumber;
    @Valid
    private List<OrderRow> orderRows = new ArrayList<>();

    public Order() {
    }
    public Order(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public Order(String orderNumber, Long id) {
        this.orderNumber = orderNumber;
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public List<OrderRow> getOrderRows() {
        return orderRows;
    }

    public void add(OrderRow orderRow) {
        if (orderRows == null) {
            orderRows = new ArrayList<>();
        }

        orderRows.add(orderRow);
    }

    public void addAll(List<OrderRow> rows) {
        if (orderRows == null) {
            orderRows = new ArrayList<>();
        }
        if (rows != null) {
            orderRows.addAll(rows);
        }
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", orderNumber='" + orderNumber + '\'' +
                ", orderRows=" + orderRows +
                '}';
    }
}
