package exservlet.dao;

import exservlet.entity.Order;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

@Repository
public class OrderDao {
    private final JdbcTemplate template;
    private final OrderRowDao orderRowDao;
    private RowMapper<Order> orderMapper = new RowMapper<Order>() {
        @Override
        public Order mapRow(ResultSet rs, int rowNum) throws SQLException {
            var order = new Order(rs.getString("order_number"));
            var id = rs.getLong("id");
            order.setId(id);
            order.addAll(orderRowDao.findAll(id));
            return order;
        }
    };


    public OrderDao(JdbcTemplate template, OrderRowDao orderRowDao) {
        this.template = template;
        this.orderRowDao = orderRowDao;
    }

    public List<Order> findAll() {
        return template.query("select * from orders", orderMapper);
    }

    public Order find(long id) {
        return template.queryForObject("SELECT * FROM orders WHERE id = ?", new Object[]{id}, orderMapper);
    }

    public void delete(long id) {
        template.update("delete from orders where id = ?", id);
    }

    public long save(Order order) {
        Map<String, Object> data = Map.of("order_number", order.getOrderNumber());
        var id = new SimpleJdbcInsert(template)
                .withTableName("orders")
                .usingGeneratedKeyColumns("id")
                .executeAndReturnKey(data).toString();

        order.getOrderRows().forEach(orderRow -> orderRowDao.save(Integer.parseInt(id), orderRow));
        return Long.parseLong(id);
    }
}

