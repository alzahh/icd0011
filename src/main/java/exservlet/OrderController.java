package exservlet;


import exservlet.dao.OrderDao;
import exservlet.entity.Order;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class OrderController {
    private OrderDao orderDao;

    public OrderController(OrderDao orderDao) {
        this.orderDao = orderDao;
    }

    @GetMapping("/orders/{id}")
    public Order getOrder(@PathVariable("id") long id) {
        return orderDao.find(id);
    }

    @GetMapping("/orders")
    public List<Order> getOrders() {
        return orderDao.findAll();
    }

    @DeleteMapping("/orders/{id}")
    public void deleteOrder(@PathVariable("id") int id) {
        orderDao.delete(id);
    }

    @PostMapping("/orders")
    public Order getOrder(@Valid @RequestBody Order order) {
        order.setId(orderDao.save(order));
        return order;
    }


}
