package exservlet;


import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


@ControllerAdvice(annotations = {RestController.class})
public class ErrorHandler {
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    @ExceptionHandler({MethodArgumentNotValidException.class, HttpMessageNotReadableException.class})
    public ErrorResponse handleBindingErrors(MethodArgumentNotValidException ex) {
        List<FieldError> fieldErrors = ex.getBindingResult().getFieldErrors();
        ErrorResponse result = new ErrorResponse();
        for (FieldError error : fieldErrors) {
            result.errors.add(new ErrorResponse.Error(error.getCode()));
        }

        return result;
    }


    public static class ErrorResponse {
        public List<Error> errors = new ArrayList<>();

        public static class Error {
            public String code;

            public Error(String code) {
                this.code = code;
            }
        }
    }
}
