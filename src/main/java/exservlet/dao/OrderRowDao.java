package exservlet.dao;

import exservlet.entity.OrderRow;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

@Repository
public class OrderRowDao {
    private final JdbcTemplate template;

    public OrderRowDao(JdbcTemplate jdbcTemplate) {
        this.template = jdbcTemplate;
    }

    private RowMapper<OrderRow> rowMapper = new RowMapper<OrderRow>() {
        @Override
        public OrderRow mapRow(ResultSet rs, int rowNum) throws SQLException {
            var row = new OrderRow();
            row.setItemName(rs.getString("item_name"));
            row.setQuantity(rs.getInt("quantity"));
            row.setPrice(rs.getInt("price"));
            return row;
        }
    };

    protected List<OrderRow> findAll(long id) {
        return template.query("select * from order_rows where order_id = ?", new Object[]{id}, rowMapper);
    }

    protected void save(int orderId, OrderRow orderRow) {
        Map<String, Object> data = Map.of("order_id", orderId, "item_name", orderRow.getItemName(),
                "quantity", orderRow.getQuantity(), "price", orderRow.getPrice());
        new SimpleJdbcInsert(template)
                .withTableName("order_rows")
                .usingGeneratedKeyColumns("row_id")
                .executeAndReturnKey(data);

    }
}